FROM openjdk:17.0.1
VOLUME /tmp
EXPOSE 8444
ARG JAR_FILE
COPY ${JAR_FILE} dms.jar
ENTRYPOINT ["java","-Dspring.profiles.active=docker","-jar","/dms.jar"]