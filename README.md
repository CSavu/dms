# DMS (Device Management Service)

## Overview

DMS is the microservice designed for managing devices within the Energy Management System and relies on the User Management Service (UMS) for user-related functionalities.
## Getting Started

### Prerequisites

Make sure you have Java, Maven, Docker, and Docker Compose installed on your machine.

- Java: [Download and Install Java](https://www.oracle.com/java/technologies/javase-downloads.html)
- Maven: [Download and Install Maven](https://maven.apache.org/download.cgi)
- Docker: [Download and Install Docker](https://www.docker.com/get-started)
- Docker Compose: [Install Docker Compose](https://docs.docker.com/compose/install/)

### Installation

1. Clone the DMS repository to your local machine.

    ```bash
    git clone https://gitlab.com/CSavu/dms.git
    ```

2. Navigate to the project directory.

    ```bash
    cd dms
    ```

3. Build the application using Maven.

    ```bash
    mvn clean install
    ```

### Dockerization

Build the Docker image using the provided Dockerfile.

```bash
docker build -t dms .
```

### Running the Application inside Docker

After building the UMS and DMS Docker images, the application can be started using Docker Compose.

```bash
docker-compose --project-name ems up -d
```