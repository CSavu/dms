package com.dms.adapters;

import com.dms.clients.UserManagementServiceClient;
import com.dms.dtos.AuthorizationResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserManagementServiceAdapter {
    @Autowired
    private UserManagementServiceClient umsClient;

    public AuthorizationResponseDto verifyToken(String token) {
        return umsClient.verifyToken(token);
    }

    public boolean doesUserExist(Integer userId, String authToken) {
        return umsClient.getById(userId, authToken) != null;
    }
}
