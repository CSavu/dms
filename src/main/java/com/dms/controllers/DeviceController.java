package com.dms.controllers;

import com.dms.entities.Device;
import com.dms.jwt.JwtUtils;
import com.dms.services.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/device")
public class DeviceController {
    @Autowired
    private DeviceService deviceService;
    @Autowired
    private JwtUtils jwtUtils;

    @GetMapping("/getAll")
    @ResponseBody
    public List<Device> getAllDevices() {
        return deviceService.getAllDevices();
    }

    @GetMapping("/getAllForUser")
    @ResponseBody
    public List<Device> getAllDevicesForUser(@RequestParam Integer userId) {
        return deviceService.getAllDevicesForUser(userId);
    }

    @GetMapping("/getById")
    @ResponseBody
    @ResponseStatus(HttpStatus.OK)
    public Device getDeviceById(@RequestParam Integer deviceId) {
        return deviceService.getById(deviceId);
    }

    @GetMapping("/getUserIdForDevice")
    @ResponseBody
    public Integer getUserIdForDevice(@RequestParam Integer deviceId) {
        return deviceService.getUserIdForDevice(deviceId);
    }

    @PostMapping("/create")
    @ResponseBody
    public Device createDevice(@Valid @RequestBody Device device) {
        return deviceService.createDevice(device);
    }

    @PatchMapping("/update")
    @ResponseBody
    public Device updateDevice(@RequestBody Device device) {
        return deviceService.updateDevice(device);
    }

    @DeleteMapping("/delete")
    @ResponseBody
    public boolean deleteDevice(@RequestParam Integer deviceId) {
        return deviceService.deleteDevice(deviceId);
    }

    @PostMapping("/addDeviceToUser")
    @ResponseBody
    public boolean addDeviceToUser(HttpServletRequest request, @RequestParam Integer deviceId, @RequestParam Integer userId) {
        String authToken = jwtUtils.parseJwt(request);
        return deviceService.createUserDeviceMapping(deviceId, userId, authToken);
    }
}
