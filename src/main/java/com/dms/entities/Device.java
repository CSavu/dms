package com.dms.entities;

import lombok.*;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@RequiredArgsConstructor
@Setter
@Getter
public class Device {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "device_generator")
    @SequenceGenerator(name="device_generator", sequenceName = "device_seq", allocationSize=1)
    private Integer id;

    @NonNull
    @NotEmpty(message = "Name cannot be empty")
    private String name;

    @NonNull
    @NotEmpty(message = "Address cannot be empty")
    private String address;

    @NonNull
    @NotNull(message = "Maximum hourly energy consumption cannot be null")
    private Double maximumHourlyEnergyConsumption;
}
