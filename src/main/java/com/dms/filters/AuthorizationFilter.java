package com.dms.filters;

import com.dms.adapters.UserManagementServiceAdapter;
import com.dms.dtos.AuthorizationResponseDto;
import com.dms.exceptions.UnauthorizedException;
import com.dms.jwt.JwtUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class AuthorizationFilter extends OncePerRequestFilter {
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;
    @Autowired
    private JwtUtils jwtUtils;
    @Autowired
    private UserManagementServiceAdapter umsAdapter;

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthorizationFilter.class);

    @Override
    @Order(2)
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            String jwt = jwtUtils.parseJwt(request);
            if (jwt == null) throw new UnauthorizedException("Invalid token");

            AuthorizationResponseDto verifyTokenResponse = umsAdapter.verifyToken(jwt);
            if (verifyTokenResponse == null || !verifyTokenResponse.isTokenValid()) {
                throw new UnauthorizedException("Invalid token");
            }
            request.setAttribute("authorizationResponse", verifyTokenResponse);

            filterChain.doFilter(request, response);
        } catch (UnauthorizedException e) {
            LOGGER.error("Cannot set user authentication: {}", e.getMessage());
            resolver.resolveException(request, response, null, e);
        }
    }
}
