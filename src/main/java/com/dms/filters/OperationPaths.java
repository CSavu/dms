package com.dms.filters;

public enum OperationPaths {
    GET_ALL_FOR_USER("/getAllForUser"),
    GET_BY_ID("/getById");

    private String path;

    OperationPaths(String path) {
        this.path = path;
    }

    public String getPath() {
        return path;
    }
}
