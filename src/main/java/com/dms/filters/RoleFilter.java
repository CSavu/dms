package com.dms.filters;

import com.dms.dtos.AuthorizationResponseDto;
import com.dms.exceptions.InvalidPropertyException;
import com.dms.exceptions.UnauthorizedException;
import com.dms.services.DeviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;
import org.springframework.web.servlet.HandlerExceptionResolver;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

import static com.dms.entities.Role.ADMIN;
import static com.dms.entities.Role.REGULAR_USER;
import static com.dms.filters.OperationPaths.GET_ALL_FOR_USER;
import static com.dms.filters.OperationPaths.GET_BY_ID;
import static com.dms.filters.UriPaths.DEVICE;

@Component
public class RoleFilter extends OncePerRequestFilter {
    @Autowired
    @Qualifier("handlerExceptionResolver")
    private HandlerExceptionResolver resolver;
    @Autowired
    private DeviceService deviceService;

    @Override
    @Order(3)
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        try {
            AuthorizationResponseDto authorizationResponse = (AuthorizationResponseDto) request.getAttribute("authorizationResponse");
            String uri = request.getRequestURI();
            String uriPrefix = uri.substring(4, uri.lastIndexOf('/'));
            String operation = request.getRequestURI().substring(uri.lastIndexOf('/'));
            String userIdAsString = request.getParameter("userId");
            String deviceIdAsString = request.getParameter("deviceId");

            if (authorizationResponse == null) {
                throw  new UnauthorizedException("You are not authorized to access this resource");
            }

            if (deviceIdAsString != null) {
                Integer deviceId = Integer.parseInt(deviceIdAsString);
                Integer userIdForDeviceId = deviceService.getUserIdForDevice(deviceId);

                if (REGULAR_USER.equals(authorizationResponse.getRole()) && DEVICE.getPath().equals(uriPrefix) && GET_BY_ID.getPath().equals(operation) && (userIdForDeviceId == null || !userIdForDeviceId.equals(authorizationResponse.getUserId()))) {
                    throw new UnauthorizedException("You are not authorized to access this resource");
                }
            }

            if (userIdAsString != null) {
                Integer userId = Integer.parseInt(userIdAsString);
                if (REGULAR_USER.equals(authorizationResponse.getRole()) && DEVICE.getPath().equals(uriPrefix) && GET_ALL_FOR_USER.getPath().equals(operation) && !userId.equals(authorizationResponse.getUserId())) {
                    throw new UnauthorizedException("You are not authorized to access this resource");
                }
            }

            if (!ADMIN.equals(authorizationResponse.getRole()) && DEVICE.getPath().equals(uriPrefix) && !(GET_ALL_FOR_USER.getPath().equals(operation) || GET_BY_ID.getPath().equals(operation))) {
                throw  new UnauthorizedException("You are not authorized to access this resource");
            }

            filterChain.doFilter(request, response);
        } catch (UnauthorizedException | InvalidPropertyException ex) {
            logger.warn("Unauthorized or invalid request");
            resolver.resolveException(request, response, null, ex);
        }
    }
}
