package com.dms.repositories;

import com.dms.entities.Device;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface DeviceRepository extends JpaRepository<Device, Integer> {
    boolean existsByNameAndAddress(String deviceName, String address);
    List<Device> findAllByNameAndAddress(String deviceName, String address);
}
