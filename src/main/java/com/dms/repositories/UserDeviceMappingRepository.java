package com.dms.repositories;

import com.dms.entities.UserDeviceMapping;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

public interface UserDeviceMappingRepository extends JpaRepository<UserDeviceMapping, Integer> {
    boolean existsByUserId(Integer userId);
    Optional<UserDeviceMapping> findByDeviceId(Integer deviceId);
    List<UserDeviceMapping> findAllByUserId(Integer userId);
    void deleteAllByDeviceId(Integer deviceId);
    void deleteAllByUserId(Integer userId);
}